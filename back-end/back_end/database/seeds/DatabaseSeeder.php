<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Str; // importare l'oggetto per le stringhe random
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0;$i<100;$i++){
            DB::table('customers')->insert(
                [
                'name' => $faker->name,
                'city' => $faker->city,
                'email' => $faker->safeEmail,
                'phone' => $faker->numberBetween($min = 100000000, $max = 9999999999),
                'age' => $faker->numberBetween($min = 18, $max = 99)
            ]);
        };
    }
}

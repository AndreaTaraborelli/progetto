<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;


class CustomersController extends Controller
{
    public function getAll(Request $request){
        $Customers = Customer::get();
        return $Customers;
    }

    public function get(Request $request, $id){
        $Customer = Customer::findOrFail($id);
        return $Customer;
    }
}
